const formulario = document.querySelector("#formulario");
const resultado = document.querySelector("#resultado");
const paginador = document.querySelector("#paginacion");



const paginacion = {
  pagina: 1,
  tamano: 20,
  paginas: 0,
  iterador: undefined
}

document.addEventListener("DOMContentLoaded", main);

function main() {
  formulario.addEventListener("submit", validarFormulario);
}

function validarFormulario(e) {
  e.preventDefault();
  const termino = document.querySelector('#termino').value.trim();

  if (termino === '') {
    alerta('Se requiere al menos una palabra clave para iniciar la busqueda');
    return;
  }
  consultar(termino);
  //consultar("Gatito");
}

function alerta(texto) {
  const existe = document.querySelector(".alerta");

  if (existe) {
    return;
  }

  const alerta = document.createElement("p");
  alerta.classList.add(
    "alerta",
    "bg-red-100",
    "border-red-400",
    "text-red-700",
    "px-3",
    "py-4",
    "rounded",
    "max-w-lg",
    "mx-auto",
    "mt-6",
    "text-center"
  );

  alerta.textContent = texto;

  formulario.appendChild(alerta);

  setTimeout(() => {
    alerta.remove();
  }, 3000);
}

function consultar() {
  
  const termino = document.querySelector('#termino').value.trim();

  const KEY = "19408612-85755b3396956a50aa3905122";
  const URI = `https://pixabay.com/api/?key=${KEY}&q=${termino}&lang=es&per_page=${paginacion.tamano}&page=${paginacion.pagina}`;

  fetch(URI)
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      paginacion.paginas = calcularPaginas(data);
      mostrarImagenes(data);
    })
    .catch((error) => {
      console.error(error);
    });
}

function mostrarImagenes({ hits }) {
  limpiarImagenes();
  hits.forEach((imagen) => {
    const { previewURL, tags, likes, views, largeImageURL } = imagen;

    const box = document.createElement("div");
    box.classList.add("w-1/2", "md:w-1/3", "lg:w-1/4", "p-3", "mb-4");

    const card = document.createElement("div");
    card.classList.add("bg-white");

    const img = document.createElement("img");
    img.classList.add("w-full");
    img.src = previewURL;
    img.alt = tags;

    const informacion = document.createElement("div");
    informacion.classList.add("p-4");
    informacion.innerHTML = `
      <p class="font-bold">${likes} <span class="font-light">me gusta<span></p>
      <p class="font-bold">${views} <span class="font-light"> visitas<span></p>

      <a
        class="block w-full bg-blue-800 text-white px-2 py-1 mt-1 rounded hover:bg-blue-400"
        href="${largeImageURL}"
        target="_blank"
        rel="noopener noreferrer">
          Ver imagen
        </a>
    `;

    box.appendChild(card);
    card.appendChild(img);
    card.appendChild(informacion);
    resultado.appendChild(box);
  });
  imprimirPaginador();
}
function limpiarImagenes() {
  while (resultado.firstChild) {
    resultado.removeChild(resultado.firstChild);
  }
}
function imprimirPaginador() {
  paginacion.iterador = crearGenerador(paginacion.paginas);
  limpiarPaginador();
  while (true) {
    const { value, done } = paginacion.iterador.next();
    if (done) { return; }
    const boton = document.createElement('a');
    boton.href = '#';
    boton.dataset.pagina = value;
    boton.textContent = value;
    boton.classList.add('siguiente', 'px-4', 'py-3', 'mx-1', 'font-bold', 'mb-2', 'uppercase', 'rounded');

    if (paginacion.pagina === value) {
      boton.style.pointerEvents = 'none';
      boton.classList.add('bg-gray-400');
    } else {
      boton.classList.add('bg-yellow-400');
    }

    boton.onclick = () => {
      paginacion.pagina = value;
      consultar();
    };

    paginador.appendChild(boton);
  }
}
function limpiarPaginador() {
  while (paginador.firstChild) {
    paginador.removeChild(paginador.firstChild);
  }
}



function *crearGenerador(total) {
  for (let i = 1; i <= total; i++) {
    yield i;
  }
}

const calcularPaginas = ({ totalHits }) => Math.ceil(totalHits/paginacion.tamano);